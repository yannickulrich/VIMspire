" Vim plugin for browsing INSPIRE-HEP
" Last Change:	2023 Sep 09
" Maintainer:	Yannick Ulrich <yannick.ulrich@durham.ac.uk>
" # Licence:	This file is licenced the GPLv3


if exists("g:loaded_vimspire")
    finish
endif
let g:loaded_vimspire = 1

command! -nargs=+ Inspire call vimspire#api#ExecuteSearchQuery(<q-args>)
