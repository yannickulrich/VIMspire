# A INSPIRE VIM plugin

vimspire is a vim plugin to search [INSPIRE-HEP](https://inspirehep.net).
One can query the database, browse the results, view abstract and references, and copy texkeys for usage in LaTeX

## Installation

vimspire can be installed using [`vim-plug`](https://github.com/junegunn/vim-plug).
Just add the following to your `vimrc`
```vimscript
    call plug#begin('~/.vim/plugged')
    Plug 'https://gitlab.com/yannickulrich/vimspire'
    " Initialize plugin system
    call plug#end()
```
and run `PlugInstall` to update.


## Usage

You can query the INSPIRE database by running `Inspire` followed by your query.
This will query the inspire database and opens a scratch buffer with the results.

## Navigating the results

vimspire displays its results in a scratch buffer.
It will include the title, authors, abstract, and references.
The latter two are hidden using folds and can be viewed with `zo` and hidden with `zc`.
The following actions exist

 * `a`:
    Open the selected reference on arXiv (if available).
    This only works on the title or author line

 * `c`:
   Inserts the texkey of the selected reference into the main buffer (if available).
   This only works on the title or author line

 * `Ctrl-]`, `Ctrl+LeftMouse`, `double LeftMouse`:
   Looks up the selected reference on INSPIRE (if possible).
   This only works on the title or author line

 * `Ctrl-t`, `BS`:
   Goes back to the previously opened results page.
