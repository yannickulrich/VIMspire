" Vim plugin for browsing INSPIRE-HEP
" Last Change:	2023 Sep 09
" Maintainer:	Yannick Ulrich <yannick.ulrich@durham.ac.uk>
" # Licence:	This file is licenced the GPLv3


" This is basic vim plugin boilerplate
let s:save_cpo = &cpo
set cpo&vim

let s:scratch_buffer = v:null
let s:scratch_window = v:null

function! vimspire#scratch#WinBufSwap()
  let thiswin = winnr()
  let thisbuf = bufnr("%")
  let lastwin = winnr("#")
  let lastbuf = winbufnr(lastwin)

  exec  lastwin . " wincmd w" ."|".
      \ "buffer ". thisbuf ."|".
      \ thiswin ." wincmd w" ."|".
      \ "buffer ". lastbuf
endfunction

func! vimspire#scratch#CreateWindow()
    split

    if s:scratch_buffer != v:null
        exec "buffer" s:scratch_buffer
    else
        noswapfile hide enew
        setlocal linebreak
        setlocal buftype=nofile
        setlocal bufhidden=hide
        setlocal nobuflisted
        setlocal foldmethod=marker
        setlocal linebreak

        call vimspire#ui#InstallShortcuts()

        let s:scratch_buffer = buffer_number()
    endif

    let s:scratch_window = win_getid(winnr("#"))

    call vimspire#scratch#WinBufSwap()
endfunc

func! vimspire#scratch#Scratch(text, ...)
    if s:scratch_window == v:null
        call vimspire#scratch#CreateWindow()
    endif
    if winbufnr(s:scratch_window) == -1
        call vimspire#scratch#CreateWindow()
    endif

    call setbufvar(s:scratch_buffer, "&modifiable", 1)
    call deletebufline(s:scratch_buffer, 1, "$")

    let l:lines = split(a:text, "\n")
    let l:i = 0
    for line in l:lines
        call appendbufline(s:scratch_buffer, l:i, l:line)
        let l:i += 1
    endfor

    call setbufvar(s:scratch_buffer, "&modifiable", 0)
    call win_execute(s:scratch_window, "normal! gg")

    if exists("a:1")
        call setbufvar(s:scratch_buffer, "state", a:1)
    endif
endfunc

" This is basic vim plugin boilerplate
let &cpo = s:save_cpo
unlet s:save_cpo
