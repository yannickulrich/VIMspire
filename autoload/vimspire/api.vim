" Vim plugin for browsing INSPIRE-HEP
" Last Change:	2023 Sep 09
" Maintainer:	Yannick Ulrich <yannick.ulrich@durham.ac.uk>
" # Licence:	This file is licenced the GPLv3


" This is basic vim plugin boilerplate
let s:save_cpo = &cpo
set cpo&vim

let g:inspire_url = "https://inspirehep.net/api/literature"
let g:inspire_opts = "fields=abstracts,titles,authors.full_name,arxiv_eprints,preprint_date,references,texkeys,reference.title,reference.arxiv_eprint&sort=mostrecent&size=25&page=1"

func! vimspire#api#CloseHandlerSearch(channel)
    let body = ""
    while ch_status(a:channel, {'part': 'out'}) == 'buffered'
        let body .= ch_read(a:channel)
    endwhile
    call vimspire#results#ShowResults(json_decode(l:body))
endfunc

func! vimspire#api#ExecuteSearchQuery(query)
    let l:url = g:inspire_url . "?q=" . substitute(a:query, " ", "%20", "g") . "&" . g:inspire_opts
    let job = job_start("curl " . l:url, {'close_cb': 'vimspire#api#CloseHandlerSearch'})
endfunc

func! vimspire#api#CloseHandlerLookup(channel)
    let l:body = ""
    while ch_status(a:channel, {'part': 'out'}) == 'buffered'
        let l:body .= ch_read(a:channel)
    endwhile
    let l:body = json_decode(l:body)
    call vimspire#results#ShowResults({"hits": {"hits": [l:body]}})
endfunc

func! vimspire#api#ExecuteLookup(id)
    let l:url = g:inspire_url . "/" . a:id . "?" . g:inspire_opts
    let job = job_start("curl " . l:url, {'close_cb': 'vimspire#api#CloseHandlerLookup'})
endfunc


" This is basic vim plugin boilerplate
let &cpo = s:save_cpo
unlet s:save_cpo
