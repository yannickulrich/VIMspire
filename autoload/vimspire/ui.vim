" Vim plugin for browsing INSPIRE-HEP
" Last Change:	2023 Sep 09
" Maintainer:	Yannick Ulrich <yannick.ulrich@durham.ac.uk>
" # Licence:	This file is licenced the GPLv3


" This is basic vim plugin boilerplate
let s:save_cpo = &cpo
set cpo&vim

func! vimspire#ui#GetOperations()
    if exists("b:state")
        return get(b:state, line("."), {})
    else
        return {}
    endif
endfunc

func! vimspire#ui#AttemptArXiV()
    let l:ops = vimspire#ui#GetOperations()
    if has_key(l:ops, "arxiv")
        let l:o = system("xdg-open https://arxiv.org/abs/" . l:ops["arxiv"])
    endif
endfunc

function! vimspire#ui#InsertText(text)
    let cur_line_num = line('.')
    let cur_col_num = col('.')
    let orig_line = getline('.')
    let modified_line =
        \ strpart(orig_line, 0, cur_col_num - 1)
        \ . a:text
        \ . strpart(orig_line, cur_col_num - 1)
    " Replace the current line with the modified line.
    call setline(cur_line_num, modified_line)
    " Place cursor on the last character of the inserted text.
    call cursor(cur_line_num, cur_col_num + strlen(a:text))
endfunction

func! vimspire#ui#AttemptTeXKey()
    let l:ops = vimspire#ui#GetOperations()
    if has_key(l:ops, "texkey")
        wincmd p
        call vimspire#ui#InsertText(l:ops["texkey"])
    endif
endfunc

func! vimspire#ui#AttemptInspire()
    let l:ops = vimspire#ui#GetOperations()
    if has_key(l:ops, "inspire")
        call vimspire#api#ExecuteLookup(l:ops["inspire"])
    endif
endfunc

func! vimspire#ui#Backwards()
    if len(g:history) > 1
        let [l:prev, l:cur] = remove(g:history, -2, -1)
        call vimspire#results#ShowResults(l:prev)
    endif
endfunc

func! vimspire#ui#InstallShortcuts()
    nmap <buffer> a :call vimspire#ui#AttemptArXiV()<CR>
    nmap <buffer> c :call vimspire#ui#AttemptTeXKey()<CR>
    nmap <buffer> <C-]> :call vimspire#ui#AttemptInspire()<CR>
    nmap <buffer> <C-LeftMouse> :call vimspire#ui#AttemptInspire()<CR>
    nmap <buffer> <2-LeftMouse> :call vimspire#ui#AttemptInspire()<CR>
    nmap <buffer> <C-T> :call vimspire#ui#Backwards()<CR>
    nmap <buffer> <BS> :call vimspire#ui#Backwards()<CR>
endfunc

" This is basic vim plugin boilerplate
let &cpo = s:save_cpo
unlet s:save_cpo
