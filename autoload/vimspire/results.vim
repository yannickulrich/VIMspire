" Vim plugin for browsing INSPIRE-HEP
" Last Change:	2023 Sep 09
" Maintainer:	Yannick Ulrich <yannick.ulrich@durham.ac.uk>
" # Licence:	This file is licenced the GPLv3


" This is basic vim plugin boilerplate
let s:save_cpo = &cpo
set cpo&vim

let g:history = []

func! vimspire#results#ShowResults(json)
    call add(g:history, a:json)

    let l:width = winwidth(winnr()) - 5
    let l:scratch = ""

    let l:operations = {}
    let l:line = 0

    for l:result in a:json['hits']['hits']
        if !has_key(l:result["metadata"], "titles")
            continue
        endif
        if !has_key(l:result["metadata"], "authors")
            continue
        endif

        let l:scratch .= repeat("-", l:width) . "\n"
        let l:scratch .= l:result["metadata"]["titles"][0]["title"] . "\n"

        let l:authors = []
        for l:author in l:result["metadata"]["authors"]
            call add(l:authors, l:author["full_name"])
        endfor
        let l:scratch .= join(l:authors, ", ") . "\n"

        let l:opts = {"inspire": l:result["id"]}
        if has_key(l:result["metadata"], "arxiv_eprints")
            let l:opts["arxiv"] = l:result["metadata"]["arxiv_eprints"][0]["value"]
        endif
        if has_key(l:result["metadata"], "texkeys")
            let l:opts["texkey"] = l:result["metadata"]["texkeys"][0]
        endif

        let l:operations[l:line+2] = l:opts
        let l:operations[l:line+3] = l:opts
        let l:line += 3

        if has_key(l:result["metadata"], "abstracts")
            let l:scratch .= "Abstract {{{\n"
            let l:scratch .= l:result["metadata"]["abstracts"][0]["value"] . "\n"
            let l:scratch .= "}}}\n"
            let l:line += 2 + len(split(l:result["metadata"]["abstracts"][0]["value"], "\n"))
        endif

        if has_key(l:result["metadata"], "references")
            let l:scratch .= "References {{{\n"
            let l:line += 1
            for l:ref in l:result["metadata"]["references"]
                let l:opts = {}
                if has_key(l:ref, "record")
                    let l:opts["inspire"] = split(ref['record']['$ref'], "/")[-1]
                endif
                if has_key(l:ref["reference"], "arxiv_eprint")
                    let l:opts["arxiv"] = l:ref["reference"]["arxiv_eprint"]
                endif

                if has_key(l:ref["reference"], "title")
                    let l:title = l:ref["reference"]["title"]["title"]
                elseif has_key(l:ref["reference"], "misc")
                    let l:title = join(l:ref["reference"]["misc"], " ")
                elseif has_key(l:ref, "raw_refs")
                    let l:title = l:ref['raw_refs'][0]['value']
                else
                    let l:title = ""
                endif
                if has_key(l:ref["reference"], "label")
                    let l:label = l:ref["reference"]["label"]
                else
                    let l:label = " "
                endif
                let l:scratch .= "[" . l:label . "] " . l:title . "\n"
                let l:operations[l:line] = l:opts
                let l:line += 1

                if has_key(l:ref["reference"], "authors")
                    let l:authors = []
                    for l:author in l:ref["reference"]["authors"][0:10]
                        call add(l:authors, l:author["full_name"])
                    endfor
                    let l:scratch .= "    " . join(l:authors, ", ") . "\n"
                    let l:operations[l:line] = l:opts
                    let l:line += 1
                endif
            endfor
            let l:scratch .= "}}}\n"
            let l:line += 1
        endif
    endfor

    call vimspire#scratch#Scratch(l:scratch, l:operations)
endfunc

" This is basic vim plugin boilerplate
let &cpo = s:save_cpo
unlet s:save_cpo
